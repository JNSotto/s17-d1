// Create Array 
// use an array literal - array literal 
const array1 = ['eat', 'sleep']; // Recommended 
console.log(array1);

//use the new keyword 
const array2 = new Array('pray', 'play');
console.log(array2);

//empty array 
const myList = [];

//array of numbers
const numArray = [2, 3, 4, 5];

//array of strings 
const stringArray =['eat', 'work', 'pray', 'play'];

//array of mixed 
const newData = ['work', 1, true];

const newData1 = [
	{'task1': 'Exercise'}, 
	[1,2,3], 
	function hello(){
		console.log('Hi I am array.');
	}
];
console.log(newData1);

 /*
Mini-Activity 
Create an array with 7 item; all strings. 
	-List seven of the places you want to visit someday
log the first item in the console
log the all the items in the console.
*/

let places = ['boracy', 'bulacan', 'Japan', 'Hongkong', 'Singapore', 'Malayasia', 'China'];
console.log(places[0]);
console.log(places);
console.log(places[places.length-1]);
console.log(places.length);
console.log(places.length-1);

for(let i=0; i < places.length; i++){
	console.log(places[i]);
}

//Array Manipulation 
//Add element to an array 

//'push()''- add element at the END of the array
let dailyActivities = ['eat', 'work', 'pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);

//unshift()- add element at the BEGINNING of the array
dailyActivities.unshift('sleep')
console.log(dailyActivities); 

//Changes element at the position of the array using INDEX
dailyActivities[2]='sing';
console.log(dailyActivities);

//Adds element at the END of the array using INDEX 
dailyActivities[6]='dance';
console.log(dailyActivities);

//Re-assign the value/items in an array: 
places[3] = 'Giza Sphinx';
console.log(places);
console.log(places[5]);
places[5] = 'Turkey';
console.log(places);
console.log(places[5]);

/*
	Re-assign the values for the first and last item in the array.
		-Re-assign it with your hometown and your highschool
	log the array in the console
	log the first and last items in the console.
 */

places[0] = 'San Mateo';
places[places.length-1] = 'St. Joseph';
console.log(places);
console.log(places[0]);
console.log(places[places.length-1])

//Adding items in an array without using methods:

let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
array[array.length-1]= 'aerith Gainsbourgh'; //it overwrite the 'tifa lockhart' element
console.log(array);
array[array.length]= 'Vincent Valentine';// it add the element at the end of the array
console.log(array);

//Array Methods 
	// Manipulate array with pre-determined JS Functions 
	// Mutators - these arrays methods usually change the original array 
	
	let array3 = ['Juan', 'Pedro', 'Jose', 'Andres']; 
	
	//without method 
	array3[array.length] = 'Francisco';
	console.log(array3);

	//.push - allows us to add an element at the end of the array
	array3.push('Andres');
	console.log(array3);

	//.unshift() - allows us to add an element at the end of the array
	array3.unshift('Simon');
	console.log(array3);

	//.pop() - allows us to delete or remove the last item/element at the end to the array
	array3.pop();
	console.log(array3);
	// it is also able to return the item we removed.
	console.log(array3.pop());
	console.log(array3);

	let removedItem = array3.pop();
	console.log(array3);
	console.log(removedItem);

	//.shift() - allows us to delete or remove the last item/element at the biginning to the array
	//.shift() - return the item we removed.
	let removedItemShift = array3.shift();
	console.log(array3);
	console.log(removedItemShift);

/*
	Mini-Activity 

		Remove the first item of array3 and log array3 in the console
		Delete the last item of array3 and log array3 in the console 
		Add "George" at the start of the array3 and Log array1 in the console
		Add "Michael" at the end of the array3 and log array1 in the console

		Use array methods.
 */
 array3.shift();
 console.log(array3);

 array3.pop();
 console.log(array3);

 array3.unshift('George');
 console.log(array3);

 array3.push('Micheal');
 console.log(array3);

 	//.sort() - by default, will allow us to sort our items in ascending order. 
 	
 	array3.sort();
 	console.log(array3);

 	let numArray1 = [3, 2, 1, 6, 7, 9];
 	numArray1.sort();
 	console.log(numArray1);

 	let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
 	numArray2.sort((a,b)=>b-a);//(a,b)=>b-a "Descending"/ (a,b)=>a-b "Ascending" - Anonymous function 
 	console.log(numArray2);
 	//.sort() convert all items into strings and then arrange the items accordingly as if they as if they are words/text
 	
 	// ascending sort per number's value
 	numArray2.sort(function(a,b){
 		return a-b
 	})
 	console.log(numArray2);

 	//descending sort per number's value
 	numArray2.sort(function(a,b){
 		return b-a
 	})
 	console.log(numArray2);

 	let arrayStr = ['Marie', 'Zen','Jamie', 'Elaine']; 
 	arrayStr.sort(function(a,b){
 		return b - a
 	})
 	console.log(arrayStr);

 	//.reverse() - reversed the order of the items
 	arrayStr.sort();
 	console.log(arrayStr);
 	arrayStr.sort().reverse();
 	console.log(arrayStr);

 	//splice() - allows us to remove and add elements from a given index. 
 	//Syntax: array.splice(startingIndex, numberofItemstobeDeleted,elementstoAdd)
 	let beatles = ['George', 'John', 'Paul', 'Ringo'];
 	let lakerPlayers = ['Lebron', 'Davis', 'Westbrook','Kobe', 'Shaq'];

 	lakerPlayers.splice(0,0,'Caruso');
 	console.log(lakerPlayers);
 	lakerPlayers.splice(0,1);
 	console.log(lakerPlayers);
 	lakerPlayers.splice(0,3);
 	console.log(lakerPlayers);
 	lakerPlayers.splice(1,1);
 	console.log(lakerPlayers);
 	lakerPlayers.splice(1,0,'Gasol', 'Fisher');
 	console.log(lakerPlayers);

 	//Non-Mutators
 		// Methods that will not change the original arrays
 		// slice() - allows us to get a potion of the original array and return a new array with the items selected from the original 
 		// syntax: slice(startIndex, endIndex)
 	
 	let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
 	computerBrands.splice(2,2,'Compaq','Toshiba','Acer')
 	console.log(computerBrands);

 	let newBrands = computerBrands.slice(1,3);
 	console.log(computerBrands);
 	console.log(newBrands);

 	let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];
 	console.log(fonts);


 	let newFontSets = fonts.slice(1,4);
 	console.log(newFontSets);

 	newFontSets = fonts.slice().reverse();
 	console.log(newFontSets);

/*
	Mini-Activity

	Given a set of data, use slice to copy the last two item in the array.
	Sava the sliced portion of the array into a new variable:
		-microsoft
	use slice to copy the third and fourth item in the array.
	Save the sliced portion of the array into a new variable:
		-nintendo
	log both new arrays in the console.

 */
let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];
let microsoft = videoGame.slice(3);
console.log(microsoft);
let nintendo = videoGame.slice(2,4);
console.log(nintendo);

// .toString() - convert the array into a single value as a string but each item will be separated by a comma
// syntax: array.toString()

let sentence = ['I', 'Like', 'JavaScript', '.', 'It', 'fun','.'];
let setenceString = sentence.toString();
console.log(sentence);
console.log(setenceString);

//.join() - converts the array into a single value as a string but separator can be specified
//syntax:array.join(separator)

let sentence2 = ['My', 'favorite','fastfood','is','Army Navy'];
let setenceString2 = sentence2.join(' ');
console.log(sentence2);
console.log(setenceString2);
let sentenceString3 = sentence2.join("/");
console.log(sentenceString3);

/*
	Mini-Activity 

	Given a set of characters: 
		- form the name, "Martin" as single string.
		- form the name, "Miguel" as single string. 
	use the method we discussed so far 

	save "Martin" and "Miguel to a variables":
		name1 and name2
	Log both variables on the console

 */
let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

/*let charArr1 = charArr.slice(5,11);
let charArr2 = charArr.slice(13,19);
console.log(charArr1);
console.log(charArr2);
let charArrStr1 = charArr1.toString();
let charArrStr2 = charArr2.toString();
console.log(charArrStr1);
console.log(charArrStr2);
let name1 = charArr1.join('');
let name2 = charArr2.join('');
console.log(name1);
console.log(name2);*/

//Solution 
let name1 = charArr.slice(5,11).join('');
let name2 = charArr.slice(13,19).join('');
console.log(name1,name2);



///.concat() - it combines 2 or more arrays without affecting the original 
//Syntax: array.concat(array1,array2)

let taskFriday = ['dring HTML', 'eat JavaScript'];
let taskSaturday = ['inhale CSS', 'breath BootStrap'];
let taskSunday = ['Get Git', 'Be Node'];

let weekendTasks = taskFriday.concat(taskSaturday,taskSunday);
console.log(weekendTasks);



//Accessors - method that allow us to access our array. 


let batch131 = [
	'Paolo',
	'Jamir',
	'Jed',
	'Ronel',
	'Rom',
	'Jayson'
];

	//indexOf()- it finds the index of the given element/item when it is first found from the left. 
	
console.log(batch131.indexOf('Jed'));//2 
console.log(batch131.indexOf('Rom'));//4



	//LastIndexof()- it finds the index of the given element/item when it is last found from the right

console.log(batch131.lastIndexOf('Jamir'));//1
console.log(batch131.lastIndexOf('Jayson'));//5

console.log(batch131.lastIndexOf('Kim'));// -1, because this item is not on the array 


/*
	Mini-Activity
	
	Given a set of brands with same entries repeated:
		Created a function which can display the index of the brand that was input the first time it was found in the array. 

		Created a function which can display the index of the brand that was input the last time it was found in the array. 
 */

		let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];

function firstcarBrands(firstBrands){
	console.log(carBrands.indexOf(firstBrands));
}

function lastcarBrands(lastBrands){
	console.log(carBrands.lastIndexOf(lastBrands));
}

firstcarBrands('Porsche');
lastcarBrands('Porsche');

// Iterator Methods - these method iteration over the items in an array much like loop 
// However, with our iterator methods there also that allows to not only iterate over items but also additional instruction. 

let avengers = [
	'Huck',
	'Black Widow',
	'Hawkeye',
	'Spider-man',
	'Iron Man',
	'Captain Americal'
];

//forEach()- similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration 

//Anonymous function within forEach will be receive each and even item in array

let marvelHereos = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHereos.forEach(function(hero){
	// iterate over all the items in Marvel Heroes array and let them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// Add an if-else wherein Cyclops and Deadpool is not allow to join
		avengers.push(hero);
	}
});
console.log(avengers);

//map() - similar to forEach however it returns new array

let number = [25, 50, 30, 10,5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
})
console.log(mappedNumbers);

//.every() - it iterates over all the items and checks if the elements passes a given condition 

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18;
});

console.log(checkAllAdult);


//.some() - it iterates over all the items check if even atleast one of the items in the array passes the condition. Same as every() it will return boolean 

let examScores = [75, 80, 74,71]; 
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >=80;
});
console.log(checkForPassing);


//.filter() - creates a new array that contains elements which passed a given condition 

let numbersArr2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0; 
});
console.log(divisibleBy5);



//.find() - it iterates over all items in our array but only returns the item that will satisfy the given condition

let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode', 'SweetChild26', 'Sundayman67'];
/*let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return username ==='mikeyTheKing2000';
});

console.log(foundUser);*/



//.includes - returns a boolean true if it finds a matching item in the array.
//Case-Sensitive

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michealKing@gmail.com',
	'pedro_himself@gmail.com',
	'sheJoneSmith@gmail.com',
	'JohnDoe@yahoo.com',
	'Sweetmarypoppies@yahoo.com'
];

// let doesEmailExist = registeredEmails.includes('michealKing1@gmail.com');
// console.log(doesEmailExist);

/*
	Mini - Activity 

	Create 2 functions 
		First functions is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second functions is able to find a specified email already exist in the registeredEmails array.
		If there is an email found, show an alert: "Email Already Exist."
		If there is no email found, show an alert: "Email is available, proceed to registration."

		You may use any of the three method we discussed recently.
 */

let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return username ==='Sundayman67';
});
console.log(foundUser);
function inputEmail(email){
	let doesEmailExist = registeredEmails.includes(email);
	if(doesEmailExist === true){
		alert("Email Already Exist.")
	}
	else {
		alert("Email is available, proceed to registration.")
	}
}

inputEmail('JohnDoe2@yahoo.com');
